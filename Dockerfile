FROM alpine:3.3

RUN apk update
RUN apk add --no-cache \
    ffmpeg=2.8.11-r0 \
    python \
    py-pip \
    py-psutil \
    python-dev \
    gcc

RUN pip install robotframework robotremoteserver

ENV DISPLAY=xvfb:0
ENV CAPTUREFILE=video1.mpg
ENV DISPLAY_SIZE=1920x1080

EXPOSE 8270

VOLUME /capture

COPY ffmpeg.sh /usr/local/bin/
COPY FfmpegControl.py FfmpegControl.py
RUN chmod ug+x /usr/local/bin/ffmpeg.sh && \
    chmod ug+x FfmpegControl.py
ENTRYPOINT ["/usr/local/bin/ffmpeg.sh"]