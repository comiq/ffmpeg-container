#!/bin/sh

case "$1" in
  remote)
    echo "Starting as Robotframework Remote Library"
    exec python FfmpegControl.py
    ;;
  *)
    echo "Starting as Standalone"
    exec ffmpeg -y -f x11grab -video_size $DISPLAY_SIZE -i $DISPLAY -r 25 /capture/$CAPTUREFILE
    ;;
esac

