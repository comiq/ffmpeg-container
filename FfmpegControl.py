from robot.api import logger
from robot.libraries.Process import Process

import time
import os
import signal

def remote_log_message(message, level, html=False):
    print '{} {}'.format(level, message)

logger.write = remote_log_message

class FfmpegControlLibrary(object):

  def __init__(self):
    self._processlib = Process()
    self.ffmpeg = None

  def start_capture(self, capturefile=os.environ["CAPTUREFILE"]):
    if self.ffmpeg:
      logger.write('{0} Ffmpeg is already running'.format(time.time() * 1000), "INFO")
    else:
      logger.write('{0} Starting Up Ffmpeg'.format(time.time() * 1000), "INFO")
      self.ffmpeg = self._processlib.start_process(
        "/usr/bin/ffmpeg -y -f x11grab -video_size {0} -i {1} -r 25 '/capture/{2}'".format(
          os.environ["DISPLAY_SIZE"],
          os.environ["DISPLAY"],
          capturefile),
        shell=True)


  def stop_capture(self):
    if self.ffmpeg and self._processlib.is_process_running(self.ffmpeg):
      logger.write("Killing Ffmpeg", "INFO")
      self._processlib.terminate_process(self.ffmpeg)
      self.ffmpeg = None
    else:
      logger.write('{0} Ffmpeg is not running'.format(time.time() * 1000), "INFO")

  def is_capturing(self):
    logger.write('{0} Checking is Ffmpeg running'.format(time.time() * 1000), "INFO")
    return self.ffmpeg and self._processlib.is_process_running(self.ffmpeg)

if __name__ == '__main__':
  import sys
  from robotremoteserver import RobotRemoteServer

  RobotRemoteServer(FfmpegControlLibrary(), host="0.0.0.0", port=8270, allow_stop=False)
